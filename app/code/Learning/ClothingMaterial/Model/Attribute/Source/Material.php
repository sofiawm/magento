<?php

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Learning\ClothingMaterial\Model\Attribute\Source;

class Material extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Fiberglass'), 'value' => 'Fiberglass'],
                ['label' => __('Steel'), 'value' => 'Steel'],
                ['label' => __('Aluminium alloy'), 'value' => 'Aluminium alloy'],
                ['label' => __('Plastic'), 'value' => 'plastic']
            ];
        }
        return $this->_options;
    }
}